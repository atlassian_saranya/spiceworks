package com.spiceworks;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import com.spiceworks.Controller.Driver;

@SpringBootApplication
public class SpiceworksApplication {

	public static void main(String[] args) {
		ConfigurableApplicationContext context = SpringApplication.run(SpiceworksApplication.class, args);
		context.getBean(Driver.class).mainMethod();
	}
}
