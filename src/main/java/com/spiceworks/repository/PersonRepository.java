package com.spiceworks.repository;

import org.springframework.data.repository.CrudRepository;
import com.spiceworks.domain.Person;

/**
 * 
 * @author selumalai
 * JPA implementation of "person" table with provides all default methods from extended CrudRepository class.
 *
 */

public interface PersonRepository extends CrudRepository<Person, Integer> {
}
