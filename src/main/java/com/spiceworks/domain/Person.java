package com.spiceworks.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
/**
 * 
 * @author selumalai
 * ORM Entity class which creates a table named "person" in embedded DB, used for data persistence.
 *
 */

@Entity
public class Person {
	@Id
	private int personId;
	private String localIdType1;
	private String localIdType2;
	private String localIdType3;

	public int getPersonId() {
		return personId;
	}

	public void setPersonId(int personId) {
		this.personId = personId;
	}

	public String getLocalIdType1() {
		return localIdType1;
	}

	public void setLocalIdType1(String localIdType1) {
		this.localIdType1 = localIdType1;
	}

	public String getLocalIdType2() {
		return localIdType2;
	}

	public void setLocalIdType2(String localIdType2) {
		this.localIdType2 = localIdType2;
	}

	public String getLocalIdType3() {
		return localIdType3;
	}

	public void setLocalIdType3(String localIdType3) {
		this.localIdType3 = localIdType3;
	}

}
