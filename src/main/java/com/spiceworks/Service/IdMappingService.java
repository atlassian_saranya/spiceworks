package com.spiceworks.Service;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.spiceworks.domain.Person;

/**
 * 
 * @author selumalai
 * Service class that contains method to read CSV file.
 *
 */

public class IdMappingService {
	public static final String DELIMTER = ",";
	public static final String EMPTY = "";
	public static final String CONNECTOR = "_";
	
	/*
	 * Method that reads csv file, creates mapping and return list of mapped persons
	 * @params filepath
	 * @returns list of mapped persons
	 */

	public List<Person> readCSVFile(String path) {
		BufferedReader br = null;
		String line = "";
		List<Person> personList = new ArrayList<Person>();
		//Map used to maintain the matches between local ids
		Map<String, Integer> identifierMap = new HashMap<String, Integer>();
		try {
			int value = 0;
			br = new BufferedReader(new FileReader(path));
			br.readLine();
			while ((line = br.readLine()) != null) {
				String[] lineEntries = line.split(DELIMTER, -1);
				List<String> list = new ArrayList<String>(
						Arrays.asList(lineEntries));
				list.removeAll(Arrays.asList("", null));

				if (list.size() > 1) {
					value++;
					for (int i = 0; i < lineEntries.length; i++) {
						value = (identifierMap.get(lineEntries[i] + "_" + i) == null) ? value
								: (identifierMap.get(lineEntries[i] + "_" + i));
					}
					for (int i = 0; i < lineEntries.length; i++) {
						if (lineEntries[i] != null
								&& !lineEntries[i].equals(EMPTY))
							identifierMap.put(lineEntries[i] + "_" + i, value);

					}
				}

			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		// Assigning id to a matched person
		Set<Integer> personSet = new HashSet<Integer>(identifierMap.values());
		for (Integer i : personSet) {
			Person person = new Person();
			person.setPersonId(i);
			for (String key : identifierMap.keySet()) {
				if (identifierMap.get(key) == i) {
					int identifierKey = Integer
							.parseInt(key.split(CONNECTOR)[1]) + 1;
					String identifierValue = key.split(CONNECTOR)[0];
					if (identifierKey == 1) {
						if (person.getLocalIdType1() == null) {
							person.setLocalIdType1(identifierValue);
						} else {
							String tempValue = person.getLocalIdType1()+", "+identifierValue;
							person.setLocalIdType1(tempValue);
						}
					}
					if (identifierKey == 2) {
						if (person.getLocalIdType2() == null) {
							person.setLocalIdType2(identifierValue);
						} else {
							String tempValue = person.getLocalIdType2()+", "+identifierValue;
							person.setLocalIdType2(tempValue);
						}
					}
					if (identifierKey == 3) {
						if (person.getLocalIdType3() == null) {
							person.setLocalIdType3(identifierValue);
						} else {
							String tempValue = person.getLocalIdType3()+", "+identifierValue;
							person.setLocalIdType3(tempValue);
						}
					}
				}
			}
			personList.add(person);		
		}
		return personList;
	}
}
