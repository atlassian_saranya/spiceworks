package com.spiceworks.Controller;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.spiceworks.Service.IdMappingService;
import com.spiceworks.domain.Person;
import com.spiceworks.repository.PersonRepository;

/**
 * 
 * @author selumalai Driver class that contain mainMethod which in turn calls
 *         service methods.
 *
 */

@Component
public class Driver {
	@Autowired
	private PersonRepository repoPerson;

	public void mainMethod() {
		System.out.println("\n\n====================Invoking IdMappingService====================");
		IdMappingService idMappingService = new IdMappingService();

		// Please update the data.csv path if needed. Absolute path is used
		// instead of relative path to make it run in all locations

		List<Person> personList = idMappingService.readCSVFile(
				"/Users/selumalai/Documents/workspace-sts-3.8.4.RELEASE/Spiceworks/src/main/java/com/spiceworks/Controller/data.csv");
		System.out.println("\nPersisting the mapped Person to Embedded DB...");
		for (Person p : personList)
			repoPerson.save(p);
		System.out.println("\nRetrieving the all the mapped Persons......");
		Iterable<Person> personDBList = repoPerson.findAll();
		for (Person person : personDBList)
			System.out.println("\npersonId - " + person.getPersonId() + "\nperson localId1 - "
					+ person.getLocalIdType1() + "\nperson localId2 - " + person.getLocalIdType2()
					+ "\nperson localId3 - " + person.getLocalIdType3());
	}
}
