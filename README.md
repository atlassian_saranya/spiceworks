# README #

This is Spiceworks Coding Exercise

### Tech Stack ###

* JAVA 8
* Springboot Framework
* Hibernate, JPA - ORM
* DB - Apache Derby (Embedded DB)
* No logging (like log4j) is used to make it lightweight app. Instead simple console output is used.


### How do I get set up? ###

To start the application
* Download the jar from the repository (Spiceworks-0.0.1-SNAPSHOT.jar in target folder) and run the following command

java -jar Spiceworks-0.0.1-SNAPSHOT.jar

* I have made the driver program to start after application launch and you can see the output as follows

personId - 1
person localId1 - abc
person localId2 - def
person localId3 - ghi

personId - 2
person localId1 - jkl
person localId2 - mno
person localId3 - pqr

personId - 3
person localId1 - stu
person localId2 - z12, vxy
person localId3 - 345

* IMPORTANT :: If you want to change the content of the file, please update the file "data.csv" located in the package "com.spiceworks.Controller". 
Absolute path is used instead of relative path, to make jar run from any location. Please update the file path if needed.

### author ###
* Saranya Elumalai (saranya.eu@gmail.com)
* contact HR : Celyna (SpiceWorks)